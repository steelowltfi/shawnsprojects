#include <Ethernet2.h>
#include <EscapeClient.h>
#include "wiring_private.h" // pinPeripheral() function


/*

3 Drawers

2017 Shawn Yates
shawn@cluecontrol.com

written and tested vMicro in Visual Studio

This is a prop controlled by MQTT messages.

The prop will have 3 game panels and 3 relay outputs.
Each game panel has an 4 position LED display and 2 buttons.
One button advances which position is being manipulated
the other button advancdes the position through a
predefined set of values.

When the proper value is set into the 4 positions, the state
of the corrosponding relay is changed.

The "proper value" is set by the incoming MQTT message that
activates the panel for play.

At startup, the prop is dectivated, idle and listening
for commands from the MQTT server.

The prop will listen for commands on one topic (see code for topic name)
Expected commands are:

1,n,xxxx,aaa			Start the game on panel n
							xxxx is the winning sequence 
							aaa is the series of characters to scroll through as the user plays
							max of 30 chars for aaa
							Note that if any char x is not included in set a, the game
							is not able to be won.  The prop will NOT check for this
							condition

2,n						Show winning combo and open drawer 'n'

3						Resets the game to be ready for next play

The prop will send updates on one topic (see code for topic name)

PanelDisplay,n,xxxx			where xxxx is the current diplayed value for the panel n
PanelSolved,n				Indicates panel n has been solved

the prop will send heartbeat messages on one topic (see code for topic name)

-------------------
*/

/*
// pin assignments
* Signal     Pin
* ------------------------
*	 0  Serial RX
*	 1  TXD to display 1 on default hardware port					
*	22	MISO - used to talk to ethernet board
*	23	MOSI - used to talk to ethernet board	
*	24	SCK - used to talk to ethernet board
*	A5	TXD to dislpay 3 - don't/can't change this pin
*	A4	
*	A3	Button A Panel 1  
*	A2	Button B panel 1
*	A1	Button A panel 2
*	A0	Button B Panel 2


*	13	Relay 1	(the onboard LED also on this pin)
*	12	Button A Panel 3
*	11	TXD to display 2 - don't/can't change this pin
*	10	CS - used to talk to ethernet board
*	 9	not used *battery charger
*	 6	Button B Panel 3
*	 5	Relay 2
*	SCL(21)	Relay 3
*   SDA(20) Lights LED

*/
// Settings likely to change/be tweaked

	char *COMMAND_TOPIC =	"/3Drawers/Commands";			//topic to listen for commands
	char *HB_TOPIC =		"/3Drawers/HeartBeat";			//topic to send heartbeats
	char *STATUS_TOPIC =	"/3Drawers/Status";				//topic to send status
	char *MY_MQTT_ID =		"3Drawers";						//id for this prop

	byte MY_MAC[] =			{ 0xDE, 0xED, 0xBA, 0xFE, 0xFE, 0xED };		//MAC address for this prop.  MUST be unique on the network

																		//note there is no '=' for creating IP addresses
	IPAddress MY_IP 		(192, 168, 1, 5);							//IP address for this prop.  MUST be unique on the network
	IPAddress BROKER_IP		(192, 168, 1, 58);							//IP address of the MQTT broker


	const long	BLINKRATE = 300;				//how many mS for the on/off cycle when blinking an LED at startup 
												//as the selected digit, or when they win

	const long KEY_REPEAT_DELAY = 500;			//how many mS does the player hold the button down before it starts
												//scrolling faster

	const long KEY_REPEAT_RATE = 10;			//how many mS between changes when scrolling in "fast scroll" mode

	const bool RELAY_ON = LOW;					//is relay active high or active low
	
// Most things below this line shouldn't be changed unless you know what you are doing.
// but feel free to read, learn and experiment.

//***************************************************************************

// Pin Renames
	byte Relays[3] = { 21,5,20 };					//array of three pins used to control the relays
	byte SelectButton[3] = { A3, A1, 12 };			//array of buttons used to select which digit is active
	byte ChangeValueButton[3] = { A2, A0, 6 };		//array of buttons used to change the selected digit
	byte ButtonLights = A4;							// drives relay to turn on button lights

// Other Constants

	const byte COMMAND_ACTIVATE = '1';			//setup commands expected over MQTT
	const byte COMMAND_OPEN = '2';				//note the single quotes store the ascii value of the char
	const byte COMMAND_RESET = '3';

	const bool RELAY_OFF = !RELAY_ON;			// off is the opposite of in, imagine that

	//game states for each drawner, for readability purposes
	const byte LED_NOT_ACTIVE = 10;
	const byte LED_BLINK_ON = 11;
	const byte LED_BLINK_OFF = 12;
	const byte LED_WON = 13;

// Function Prototypes

	bool DebounceSW(byte SWx);											//Button debounce function
	void RX_MQTT(char *topic, byte *payload, unsigned int length);		//Callback for MQQT receive
	void ActivateDisplay(byte Unit);									//Start game play on a drawer unit
	void ResetGame();													//reset the game back to ready to play state
	void CheckWinner(byte Unit);										//check to see if a game has been won
	void ForceWinner(byte Unit);										//to force winning state based on MQTT command
	void ReportWinner(byte Unit);										//report winner to MQTT server

	void UpdateLED(byte Unit);											//Display current value on the indicated LED
	void ClearLED(byte Unit);											//clear (turn off) indicated LED
	void ClearDigit(byte Unit, byte Digit);								//clear (turn off) indicated digit on indicated LED
	void CommandLED(byte Unit, char * Command);							//send commands to the indicated LED
	void CelebrateWin(byte Unit);										//display something cool when they win	


//Class Setup
	EthernetClient ethClient;											
	PubSubClient pubSubClient(BROKER_IP, 1883, ethClient);				//create MQTT client with broker ip and default port of 1883
	EscapeClient MQTTClient = EscapeClient();							//Create a client using the SteelOwl MQTT wrapper
		
	//create UART ports for talking to the LED displays
	// (Sercom funcions, RX_PIN, TX_PIN, RX_PAD, TX_PAD)
	// see https://learn.adafruit.com/using-atsamd21-sercom-to-add-more-spi-i2c-serial-ports/creating-a-new-serial
	// for details

	// note that only the TX pin and pad will be used, the RX
	// will not be used
	Uart LED2(&sercom1, 13, 11, SERCOM_RX_PAD_1, UART_TX_PAD_0);		
	Uart LED3(&sercom5, 3, A5, SERCOM_RX_PAD_1, UART_TX_PAD_0);
	
	//for consistency, rename Serial1 to LED1 so code can say LED1
	#define	LED1	Serial1
	

//Variables
	
	char Winners[3][4] = { {'1','2','3','4'},							//winning combos for each drawer
						   {'1','2','3','4'},							//will be changed by MQTT activation command
						   {'1','2','3','4'} };		

	char Sequence[3][30] = { '0' };										//holder for the sequences to scroll through
																		//will be defined by MQTT activation command

	byte CurrentValue[3][4];											//hold the index of the currently diplayed
																		//character for each digit of each display


	byte CurrentDigit[3];												//used to store character/state of each display
																		//is currently selected.  Will also be used to 
																		//indicate various states by storing values
																		//0-3 = digit selection (blinking on)
																		//4-7 = digit selection (blinking off)
																		//10 = Display idle - not active yet
																		//11 = display just started, digits flashing ON
																		//12 = display just started, digits flashing OFF
																		//13 = drawer won, game inactive, maglock off

	bool ButtonState[3][2];												//used to track the last known state of each button
																		//to prevent re-trigger of teh select button
																		//and to track hold time for value button to enable
																		//fast scrolling

	unsigned long NextLEDEvent[3];										//used to store future millis value for a given LED
																		//to take another action (blink on/off)

	unsigned long NextButtonEvent[3];									//used to time the holding of the digit value button
	bool FastScroll[3];													//indicate if an LED is in fast scroll mode

	char MQTTStatus[10];												//used to send status message to MQTT

	byte	WorkingValue;												//used for readability in code
	byte	WorkingDigit;

void setup()
{

	// serial setup
	Serial.begin(9600);
	delay(50);
	Serial.println("Serial interface started");	

	MQTTClient.setup(pubSubClient, MY_MAC, MY_IP, MY_MQTT_ID, HB_TOPIC);	//setup the call back for
	Topic t = {COMMAND_TOPIC,RX_MQTT};										//create the topic subscription
	MQTTClient.registerChannel(t);											//and register to monitor it

	Serial.print("Expecting dispatcher on ");
	Serial.println(BROKER_IP);

	
	LED1.begin(115200);					//start the LED interfaces
										//default speed for the LED is 115,200
	LED2.begin(115200);
	pinPeripheral(11, PIO_SERCOM);		//for manually created ports, need to assign pin functionality
	
	LED3.begin(115200);
	pinPeripheral(A5, PIO_SERCOM_ALT);


	LED1.print("\r");					//send CR to clear buffer of LEDs
	LED2.print("\r");
	LED3.print("\r");

	LED1.print("level 255\r");			//set brightness as high as possible
	LED2.print("level 255\r");
	LED3.print("level 255\r");

	LED1.print("splash off\r");			//Turn off power-up splash screen display
	LED2.print("splash off\r");
	LED3.print("splash off\r");
		
	//	LED1.print("print \"1-");
	//	LED1.print(x + 64);
	//	LED1.print("\"\r");

	for (byte x = 0; x < 3; x++)
	{
		pinMode(SelectButton[x], INPUT_PULLUP);
		pinMode(ChangeValueButton[x], INPUT_PULLUP);
		pinMode(Relays[x], OUTPUT);						//make each relay an output
	}

	pinMode(ButtonLights, OUTPUT);						//set button light relay as output

	ResetGame();
	
	while (DebounceSW(SelectButton[0]))				//do power on self test as long as select button 0 is held
	{
		Serial.println("Self Test Initiated");

		//48 - 57 are numbers 0 to 9
		//65 - 90 are letters A to Z
		for (byte x = 48; x < 58; x++)					//display digits on all LEDs
		{
			char cmd[14] = "print \"xxxx\"\r";			//setup the command structure
			for (byte y = 7; y < 11; y++) 
			{
				cmd[y] = char(x);					//replace the x's with the numbers
			}

			Serial.println(cmd);

			for (byte y = 0; y < 4; y++)	
			{
				CommandLED(y, cmd);						//send the command to each display panel
			}
			delay(400);
		}

		for (byte y = 0; y < 3; y++)
		{
			digitalWrite(Relays[y], RELAY_ON);			//turn on all drawer relays
			delay(100);
		}
		
		digitalWrite(ButtonLights, RELAY_ON);			//turn on the button light relay
		

		//65 - 90 are letters A to Z
		for (byte x = 65; x < 91; x++)					//display letters on all LEDs
		{
			char cmd[14] = "print \"xxxx\"\r";			//setup the command structure
			for (byte y = 7; y < 11; y++)
			{
				cmd[y] = char(x);					//replace the x's with the letters
			}

			Serial.println(cmd);
			for (byte y = 0; y < 4; y++)
			{
				CommandLED(y, cmd);						//send the command to each display panel
			}
			delay(400);
		}


		ResetGame();									//turn off all lights and relays
		
	}
			
	Serial.println("Starting Main Loop");
}

void loop()
{
	MQTTClient.loop();					//service the MQTT client.  Keep in main loop all the time

	for (byte x = 0; x < 3; x++)		//loop through each game and service as needed
	{
		switch (CurrentDigit[x])
		{
			case LED_NOT_ACTIVE:		//if the LED is not active, there is nothing else to do, move on
				continue;				//to the next one
			
			case 0: case 1: case 2: case 3:					//if any digit is selected, turn LED off
				if (millis() > NextLEDEvent[x])				//when NextEvent happens - blinking the selected
				{											//digit
					ClearDigit(x, CurrentDigit[x]);			//by turning it off here and back on below
					NextLEDEvent[x] = millis() + BLINKRATE;
					CurrentDigit[x] = CurrentDigit[x]+4;	//now it will be 4-7 and use the case below
				}
				break;


			case 4: case 5: case 6: case 7:
				if (millis() > NextLEDEvent[x])				//when NextEvent happens - blinking the selected
				{											//digit
					UpdateLED(x);							//turning it on here and off above
					NextLEDEvent[x] = millis() + BLINKRATE;
					CurrentDigit[x] = CurrentDigit[x] - 4;	//now it will be 0-4 and use the case above
				}
				break;

			case LED_BLINK_ON:								//if the LED is on,
				if (millis() > NextLEDEvent[x])				//and NextEvent time has passed
				{
					
					ClearLED(x);							//turn the LED off
					NextLEDEvent[x] = millis() + BLINKRATE;	//set teh the NextEvent time
					CurrentDigit[x] = LED_BLINK_OFF;		//and the state, so tehy will be turned
				}											//back on when time passes.
				break;

			case LED_BLINK_OFF:								//if LED is off
				if (millis() > NextLEDEvent[x])				//and NextEvent time has passed
				{
					UpdateLED(x);							//turn the LED on
					NextLEDEvent[x] = millis() + BLINKRATE;	//set teh the NextEvent time
					CurrentDigit[x] = LED_BLINK_ON;			//and the state, so tehy will be turned
				}											//back off when time passes.
				break;

			case LED_WON:
				continue;
			
		}//end of swith based on state
		/************************************************************/
		//note that code below here that is inside the "looping through game"
		//will not execute for LEDs that are in the not active state
		//because of the "continue" statement in the not active case above

		//check the state of the buttons
	
		//if button a is pressed, change to the next digit, don't do this again until
		//the buton is first released
		if (DebounceSW(SelectButton[x]) != ButtonState[x][0])		//if the button is other than the state
		{															//it used to be, process the state change

			
			ButtonState[x][0] = !ButtonState[x][0];					//the new state must be the opposite of the old

			if (ButtonState[x][0])									//if button is now pressed, increment digit
			{
				if (CurrentDigit[x] > 9) CurrentDigit[x] = 255;		//if this is the first button push, make sure 
																	//the ++ below starts at 0

				CurrentDigit[x]++;									//increment current digit
				if (CurrentDigit[x] > 3 ) CurrentDigit[x] -= 4;		//if its over 3, subtract 4
			}
		}

		//if button b is pressed, increment the digit value.  If the button is held,
		//increment the values faster until the button is released
		//when the button is released (afer a long or short press) send thew new current
		//value to the MQTT dispatcher  Also, check for a winning combination

		//if the CurrentDigit is >7, no digit is selected yet, don't check the value button
		//added this line 5/15 to resolve issue where pushing value button on one display
		//changes the value on another display.
		if (CurrentDigit[x] > 7) continue;

		if (DebounceSW(ChangeValueButton[x]) != ButtonState[x][1])		//if the button has changed state, process it
		{
			ButtonState[x][1] = !ButtonState[x][1];					//toggle the state indicator
	

			if (ButtonState[x][1])									//button is now pressed
			{
				WorkingDigit = CurrentDigit[x];									//get the current digit, 
				if (CurrentDigit[x] > 3) WorkingDigit = CurrentDigit[x] - 4;	//if its 4-7 (because of blinking)
																				//reduce it by 4 to get the real digit value	


				WorkingValue = CurrentValue[x][WorkingDigit];		//store the current value in working value for easy reading
				WorkingValue++;										//increment the current value
				CurrentValue[x][WorkingDigit] = WorkingValue;		//save the incremented value	
										

				if (Sequence[x][WorkingValue] == 0)					//now, look at the character that current value 
				{													//points to in the sequence.	
					CurrentValue[x][WorkingDigit] = 0;				//held in Sequence[LED#][Index]
																	//and index = CurrentValue[LED#][Digit#]  
																	//if the char value is 0, end of the sequence
				}													//go back to index 0
																		

				NextButtonEvent[x] = millis() + KEY_REPEAT_DELAY;			//assume the next repeat is "key_repeat_delay" away
				if (FastScroll[x])											//but if fast scroll is already enabled
				{
					NextButtonEvent[x] = millis() + KEY_REPEAT_RATE;		//use the shorter delay instead
				}
				

			}//end of if button has become activated
			else
			{															//button is deactivated.  Send update to MQTT
				MQTTStatus[0] = x + 49;									//add 49 to get the ascii value of the unit number
				MQTTStatus[1] = ',';
				for (byte i = 0; i < 4; i++)
				{
					MQTTStatus[i+2] = Sequence[x][CurrentValue[x][i]];	//build the status message with the current values
				}														//of the sequence
				MQTTStatus[7] = 0;										//null terminate the string

				MQTTClient.publish(STATUS_TOPIC, MQTTStatus);			//send the message;
				FastScroll[x] = false;									//make sure fast scroll is turned off

				CheckWinner(x);
			}//end of if button has become deactivated

		}//end of if button state changed

		if (ButtonState[x][1])											//if the value button is active
		{
			UpdateLED(x);												//keep the LED on when changing  it

			if (millis() > NextButtonEvent[x])								//and the right amount of time has passed
			{
				ButtonState[x][1] = !ButtonState[x][1];					//toggle the state, which will force it to be re-detected
				FastScroll[x] = true;									//as a button push next time around.  Set fast scroll
			}															//so a shorter time will be used to repeat it
		}




	}// end of for looping through each game


}  //end of main routine


void ActivateDisplay(byte Unit)
{
	//Start gameplay for the indicated unit.
	//The sequence value have to have been set already

	for (byte x = 0; x < 4; x++)
	{
		CurrentValue[Unit][x] = 0;						//reset each digit value back to 0
	}
	CurrentDigit[Unit] = LED_BLINK_ON;					//set teh mode to be blinking
	UpdateLED(Unit);									//show the digits
	NextLEDEvent[Unit] = millis() + BLINKRATE;			//set teh timer for the blinking
	digitalWrite(ButtonLights, RELAY_ON);
}

bool DebounceSW(byte SWx)
{
	//Read the switch 5 times, 10mS apart.  Return 
	//the value of the majority of the reads.
	//this is to prevent false or erattic triggering
	//caused by the internal mechanical bounce of a switch

	byte HighCounts = 0;

	for (int x = 0; x <5; x++)
	{
		if (!digitalRead(SWx))    //invert the reading due to the use of pullups
		{
			HighCounts++;		//increment the high counter	
		}

		delay(10);				//wait 10 mS before looping again
	}
	return (HighCounts > 2);    //if there are more than 2 hights, return high
}

void RX_MQTT(char *topic, byte *payload, unsigned int length) 
{

	char	Command;
	char	Unit;
		
	Serial.print("RXD Topic:   ");			//show the topic just for troubleshooting
	Serial.println(topic);
	Serial.print("Payload:  ");				//show the payload for troubleshooting

	for (int i = 0; i < length; i++) {		
		Serial.print((char)payload[i]);		//loop through the payload, casting each byte to a char
	}										//to print it
	Serial.println("");

	Command = payload[0];

	switch (Command)
	{
	case COMMAND_ACTIVATE:
		
		//structure:  C,n,xxxx,aaaa  C = command byte   n = unit number  xxxx = winning combo,
									 //aaa = sequence to rotate (up to 50 chars)

		Serial.println("Activate Command Received:");
		Serial.print("     Drawer:    ");

		
		Unit = payload[2] - 49;			//get the unit nubmer, convert to actual value instead of a char
		Serial.println((byte)Unit);		//by subtracting ascii 0 from the value.  print using
										//byte to make sure the calc was correct


		Serial.print("     To Win:    ");
		for (byte x = 0; x < 4; x++)				//copy the winning sequence from payload
		{											//into winners array
			Winners[Unit][x] = payload[x + 4];		
			Serial.print((char)Winners[Unit][x]);	//this time, make sure the print out is using chars
		}											//as there can be letters and numbers
		Serial.println();
		

		Serial.print("     Sequence:  ");
		for (int i = 9; i < length; i++) 
		{
			Sequence[Unit][i - 9] = payload[i];
			Serial.print((char)Sequence[Unit][i - 9]);	
			if (i == 39) { break; }
		}

		Sequence[Unit][length - 9] = 0;		//mark the end with a 0
		Serial.println(".");
		

		ActivateDisplay(Unit);

		break;

	case COMMAND_OPEN:

		//structure:  C,n		C = command byte   n = unit number to open

		Serial.println("Open Command Received:");
		Serial.print("     Drawer:    ");


		Unit = payload[2] - 49;			//get the unit nubmer, convert to actual value instead of a char
		Serial.println((byte)Unit);		//by subtracting ascii 0 from the value.  print using
										//byte to make sure the calc was correct

		ForceWinner(Unit);
		break;

	case COMMAND_RESET:
		Serial.println("Reset Command Received");
		ResetGame();
		break;
	}



	
}

void ResetGame()
{	//reset the game to be ready to play.  Called at startup, and
	//in response to the reset command over MQTT

	LED1.print("clear\r");				//make sure the displays are blank/off
	LED2.print("clear\r");
	LED3.print("clear\r");

	for (byte x = 0; x < 3; x++)				//set each LED set to inactive state
	{
		CurrentDigit[x] = LED_NOT_ACTIVE;
		digitalWrite(Relays[x], RELAY_OFF);		//turn off all the relay outputs
	}

	digitalWrite(ButtonLights, RELAY_OFF);		//turn off the button lights
}

void ClearLED(byte Unit)											//clear (turn off) indicated 
{
	char cmd[] = "clear\r";
	CommandLED(Unit, cmd);
}


void ClearDigit(byte Unit, byte Digit)								//clear (turn off) indicated digit on indicated LED
{
	char cmd[14] = "print \"xxxx\"\r";

	for (byte x = 0; x < 4; x++)									//x will represent which digit is being addressed
	{

		cmd[x + 7] = Sequence[Unit][CurrentValue[Unit][x]];			//get the value for each digit
		if (x == Digit) cmd[x + 7] = ' ';							//if the current digit is the one to turn off
																	//insert a space to make it blank
																	//CurrentValue[Unit][x] holds the index of
																	//the digit to be displayed
																	//Sequence[unit] holds all the values
																	//so Sequnce[Unit][n] dislays value n
																	//of the sequence.  In this case,
																	//n = CurrentValue[Unit][x]
	}

	CommandLED(Unit, cmd);


}


void UpdateLED(byte Unit)
{	//write to the selected LED
	//must write all 4 digits
	//command is print "xxxx"<CR>
	//must include the "

	char cmd[14] = "print \"xxxx\"\r";

	for (byte x = 0; x < 4; x++)									//x will represent which digit is being addressed
	{

		cmd[x + 7] = Sequence[Unit][CurrentValue[Unit][x]];			//get the value for each digit
																	//CurrentValue[Unit][x] holds the index of
																	//the digit to be displayed
																	//Sequence[unit] holds all the values
																	//so Sequnce[Unit][n] dislays value n
																	//of the sequence.  In this case,
																	//n = CurrentValue[Unit][x]
	}

	CommandLED(Unit, cmd);
	
}
	
void CommandLED(byte Unit, char * Command)
{
	//Serial.print("Sending command ");									//now send the command, and echo
	//Serial.print(Command);												//it to the serial for debug and tracing
	switch (Unit)
	{
		case 0:
			LED1.print(Command);
			//Serial.println(" to LED1.");
			break;
		case 1:
			LED2.print(Command);
			//Serial.println(" to LED2.");
			break;
		case 2:
			LED3.print(Command);
			//Serial.println(" to LED3.");
			break;
	}
	


}

void ForceWinner(byte Unit)
{		//force the indicated unit to a winning state by looping through
		//the winning combo and then searching the sequence for each
		//winning char.

	char	NextChar;
	byte	NextIndex = 0;
	bool	Found;

	for (byte i = 0; i < 4; i++)						//loop through each winning char and find it in sequence
	{

		Found = false;									//clear the found flag	
		NextIndex = 0;									//start at index 0
		NextChar = Sequence[Unit][NextIndex];			//start at the first char in the sequence

		while ((NextChar != 0) & !Found)				//loop till we hit a null or the value is found
		{

			if (Winners[Unit][i] == NextChar)			//if the value is found
			{
				CurrentValue[Unit][i] = NextIndex;		//store the index so the char can be displayed
				Found = true;							//and set found to true so the loop exits

			}
			NextIndex++;
			NextChar = Sequence[Unit][NextIndex];
		}

	}

	CheckWinner(Unit);

}

void CheckWinner(byte Unit) 
{		//check each char of the indicated unit to see if teh player won

		//note that the winners array is holding chars, so these must be 
		//matched up to the chars in the sequence array

	for (byte i = 0; i < 4; i++)			//go through the 4 digits.  
	{										

		//CurrentValue[Unit][i] returns the index of teh currently displayed value
		//in the sequence.  If that does not match the same value in winners, exit
		if (Sequence[Unit][CurrentValue[Unit][i]] != Winners[Unit][i]) return;
	}
	
	//if the code above does not execute the return. all digits match, we have a winner!

	
	CurrentDigit[Unit] = LED_WON;						//and set the state to won

	digitalWrite(Relays[Unit], RELAY_ON);
	CelebrateWin(Unit);
	ReportWinner(Unit);
	UpdateLED(Unit);									//update the display

}

void ReportWinner(byte Unit)
{	//send in the winning MQTT report
	char Stat[] = "x,WON";

	Stat[0] = Unit + 49;									//add 49 to get the ascii value of the unit number


	MQTTClient.publish(STATUS_TOPIC, Stat);			//send the message;

}

void CelebrateWin(byte Unit)

{	//Caution:  blocking code

	//dislplay a little celbration for the players when they win 
	// a drawer

	//Map to decimal segment values:

	//    --3278--   --16384--
	//   | \       |       / |
	//	 |  \      |     /   |
	//  256   128  64   32   8192
	//   |     \   |   /     |
	//   |      \  |  /      |
	//   ---1-----  -----16---
	//   |       / | \       |
	//   |      /  |  \      |
	//  512   2    4   8    4096
	//   |   /     |    \    |
	//   |  /      |     \   |
	//   --1024---   --2048---

	//add all the values to turn on, make a hex word
	//commant to send:
	//raw aaaa,bbbb,cccc,dddd
	// where aaaa is the hex sum of all segments to turn on for digit 1
	//       bbbb is the hex sum of all segments to turn on for digit 2...

	//values here were calculated in excel and typed in

	byte dly = 100;
	
		CommandLED(Unit, "raw 0080,0000,0000,0000\r");
		delay(dly);

		CommandLED(Unit, "raw 0040,0080,0000,0000\r");
		delay(dly);

		CommandLED(Unit, "raw 0020,0040,0080,0000\r");
		delay(dly);

		CommandLED(Unit, "raw 0010,0020,0040,0080\r");
		delay(dly);

		CommandLED(Unit, "raw 0008,0010,0020,0040\r");
		delay(dly);

		CommandLED(Unit, "raw 0004,0008,0010,0020\r");
		delay(dly);

		CommandLED(Unit, "raw 0002,0004,0008,0010\r");
		delay(dly);

		CommandLED(Unit, "raw 0001,0002,0004,0008\r");
		delay(dly);

		CommandLED(Unit, "raw 00ff,0001,0002,0004\r");
		delay(dly);

		CommandLED(Unit, "raw 00ff,00ff,0001,0002\r");
		delay(dly);

		CommandLED(Unit, "raw 00ff,00ff,00ff,0001\r");
		delay(dly);

	for (byte x = 0; x < 4; x++)
	{
		CommandLED(Unit, "raw 00ff,00ff,00ff,00ff\r");
		delay(dly);

		CommandLED(Unit, "raw 0000,0000,0000,0000\r");
		delay(dly);

	}




	ClearLED(Unit);

	




}