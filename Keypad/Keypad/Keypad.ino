

#include <PubSubClient.h>
#include <Ethernet2.h>
#include <EscapeClient.h>
#include <Tones.h>

/*
 NOTE
 
 The keypad library had to be modified - it declared a macro OPEN
 and so does another library.  This caused a failure to compile.
 
 Be sure to update the keypad library - change the OPEN to kpOPEN
 and change CLOSED to kpCLOSED.
 
 */
#include <Keypad.h>
#include <Key.h>

/*

Keypad Prop

2017 Shawn Yates
shawn@cluecontrol.com

written and tested vMicro in Visual Studio

This is a prop controlled by MQTT messages.

The prop will have a kepyad with relays and an
indicator light.  Everything starts turned off.
When an MQTT start command is received, the indicator will
turn on and if the players enter a correct code as defined
by the MQTT command, a corrosponding relay will turn on.

At startup, the prop is dectivated, idle and listening
for commands from the MQTT server.

The prop will listen for commands on one topic (see code for topic name)
Expected commands are:

1,n,xxxx				Start the game
						Set code for relay n to xxxx
						Relay number can be 1 to 4
						xxxx is the winning sequence (max of 8 digits)
						its ok to call this multiple times with different
						codes to set each relay.  Calling it again with the 
						same relay will overwrite the previous code for that
						relay

						
						Note that no matter what is printed on the keypad
						it is assuemed to have this layout:
						1 2 3 A
						4 5 6 B
						7 8 9 C	
						* 0 # D

						Using a keypad that has less columns or less rows
						will work, just be sure not to include those chars
						in the winning sequence

2,n						Force relay 'n' to open

3						Resets the game to be ready for next play

The prop will send updates on one topic (see code for topic name)

1,n							where n is the digit pressed
2,n							Indicates relay n has been activated
							either by proper code or by override command

the prop will send heartbeat messages on one topic (see code for topic name)

-------------------
*/

/*
// pin assignments
* Signal     Pin
* ------------------------
*	 0  Piezo Output
*	 1  Indicator Output
*	22	MISO - used to talk to ethernet board
*	23	MOSI - used to talk to ethernet board
*	24	SCK - used to talk to ethernet board
*	A5	COL-3
*	A4  COL-4 (optional)
*	A3	ROW-D (2)
*	A2	ROW-C (3)
*	A1	COL-1
*	A0	COL-2


*	13	Relay 1	(the onboard LED also on this pin)
*	12	ROW-B (2)
*	11	ROW-A (1)
*	10	CS - used to talk to ethernet board
*	 9	not used *battery charger
*	 6	Relay 2
*	 5	Relay 3
*	SCL	Relay 4

*/
// Settings likely to change/be tweaked

char *COMMAND_TOPIC = "Keypad/Commands";			//topic to listen for commands
char *HB_TOPIC = "Keypad/HeartBeat";			//topic to send heartbeats
char *STATUS_TOPIC = "Keypad/Status";				//topic to send status
char *MY_MQTT_ID = "Keypad";						//id for this prop

byte MY_MAC[] = { 0xDE, 0xED, 0xAB, 0xFE, 0xFE, 0xED };		//MAC address for this prop.  MUST be unique on the network

															//note there is no '=' for creating IP addresses
IPAddress MY_IP(192, 168, 1, 6);							//IP address for this prop.  MUST be unique on the network
IPAddress BROKER_IP(192, 168, 1, 58);							//IP address of the MQTT broker

	const bool INDICATOR_ON = HIGH;					//is the indicator active high or active low?  
	const bool RELAY_ON = HIGH;						//are the relays active high or active low?


	TONES	Tones;
	const unsigned int TONE_PRESS = Tones.NOTE_AS2;		//tone to play with each button pressed
	const unsigned int TONE_OPEN = Tones.NOTE_AS5;		//tone to play when right code is entered
	const unsigned int TONE_WRONG = Tones.NOTE_A1;		//tone to play when wrong code is intered
	
	const int TONE_OPEN_LEN = 250;					//how many mS to play open tone
	const int TONE_PRESS_LEN = 50;					//how many mS to play press tone
	const int TONE_WRONG_LEN = 150;				//how many mS to play wrong tone
	


//***************************************************************************//
	
// Most things below this line shouldn't be changed unless you know what you are doing.
// but feel free to read, learn and experiment.

//***************************************************************************//

// Pin Renames
const byte INDICATOR = 1;								//indicator output to show game is active	
const byte PIEZO = 0;									//piezo output for audio feedback to players
const byte RELAYS[4] = { 13,6,5,21 };					//array of three pins used to control the relays


//Kepyad Setup
const byte Rows = 4;
const byte Cols = 4;
char keys[Rows][Cols] = {
	{'1','2','3','A'},
	{'4','5','6','B'},
	{'7','8','9','C'},
	{'*','0','#','D'} };

byte rowPins[Rows] = { 11,12,A2,A3 };				//if wiring to keypad changes, change these to match new wiring (row1, row2, row3, row4)
byte colPins[Cols] = { A1, A0, A5, A4 };			//col1 col2 col3 col 4
Keypad MyKeypad(makeKeymap(keys), rowPins, colPins, Rows, Cols);

// Other Constants

const bool INDICATOR_OFF = !INDICATOR_ON;
const bool RELAY_OFF = !RELAY_ON;

const byte COMMAND_ACTIVATE = '1';			//setup commands expected over MQTT
const byte COMMAND_OPEN = '2';				//note the single quotes store the ascii value of the char
const byte COMMAND_RESET = '3';

// Function Prototypes

bool DebounceSW(byte SWx);											//Button debounce function
void RX_MQTT(char *topic, byte *payload, unsigned int length);		//Callback for MQQT receive
void ReportWinner(byte Unit);										//report winner to MQTT server
void ResetGame();													//to reset the game to be ready to play
void FindMax();														//Determine the length of the longest active winning relay seq

					
//Class Setup
EthernetClient ethClient;
PubSubClient pubSubClient(BROKER_IP, 1883, ethClient);				//create MQTT client with broker ip and default port of 1883
EscapeClient MQTTClient = EscapeClient();							//Create a client using the SteelOwl MQTT wrapper


//Variables

char Winners[4][9] = { { '1','2','3','4','5','6','7','8','A' },			//winning combos for each drawer
{ '1','2','3','4','5','6','7','8','A' },								//will be changed by MQTT activation command
{ '1','2','3','4','5','6','7','8','A' },
{ '1','2','3','4','5','6','7','8','A' } };


char MQTTStatus[10];												//used to send status message to MQTT

bool Active[4];														//track if each relay is in play or not
byte RightCount[4];													//track how many correct key presses for each relay sequence

char NextKey;														//store which character is sent by the keypad
byte NextTarget;													//store which index of Winners is expected next
byte PressCount;													//count how many keys the player has pressed in this round_error
byte MaxLength;														//store the length of the longest winning sequence for an active relay


void setup()
{

	// serial setup
	Serial.begin(115200);
	Serial.println("Serial interface started");

	

	MQTTClient.setup(pubSubClient, MY_MAC, MY_IP, MY_MQTT_ID, HB_TOPIC);	//setup the call back for
	Topic t = { COMMAND_TOPIC,RX_MQTT };									//create the topic subscription
	MQTTClient.registerChannel(t);											//and register to monitor it

	pinMode(INDICATOR, OUTPUT);												//setup the indicator output
	digitalWrite(INDICATOR, INDICATOR_OFF);
	
	pinMode(PIEZO, OUTPUT);													//setup the piezo pin
	digitalWrite(PIEZO, LOW);

	for (byte x = 0; x < 4; x++)
	{
		pinMode(RELAYS[x], OUTPUT);
		digitalWrite(RELAYS[x], RELAY_OFF);
	}

	ResetGame();															//set the game to a known starting point

}

void loop()
{
	MQTTClient.loop();					//service the MQTT client.  Keep in main loop all the time
	NextKey = MyKeypad.getKey();		//see if any key is pressed
	if (NextKey == NO_KEY)
	{
		return;		//if no key is pressed, go back to beginning of main loop
	}

	//a key was pressed, process it!
	tone(PIEZO,TONE_PRESS,TONE_PRESS_LEN);				//play the keypress tone
	PressCount++;										//Increment the press counter
	//Serial.print(NextKey);							//un-comment this line to see each keypress in serialmonitor
	
	for (byte CurRelay = 0; CurRelay < 4; CurRelay++)
	{
		if (!Active[CurRelay])continue;					//if the current relay is not in play, continue to the next relay

														//if players are entering the right codes, the RightCount will serve as 
														//the index to the winners array for the next expected character
		NextTarget = RightCount[CurRelay];				//get the index of the next char for this relay (just for readability)

		if (NextKey != Winners[CurRelay][NextTarget])	//if the key pressed is not the next key for this relay
		{
			RightCount[CurRelay] = 0;					//reset the right count for this relay to 0
			continue;									//andn go to the next relay
		}

		//the key pressed was the right next key for this relay.
		RightCount[CurRelay]++;							//increment the right counter for this relay
		NextTarget = RightCount[CurRelay];				//put new index in NextTarget for readability

		if (Winners[CurRelay][NextTarget] == 0)			//if the next target is 0, this relay is won!
			ReportWinner(CurRelay);						//Process the winner.
														//note that report winner will 0 out PressCount
		
	}// end of looping through each relay
	
	if (MaxLength == 0) return;							//if all relays are now won, maxlen will be 0
														//skip checking max len to prevent an extra "wrong"
														//tone from sounding

	if (PressCount >= MaxLength)						//if players have entered teh number of keys present in the 
	{													//longest winning sequence and have not won (because winning resets
														//PressCount to 0),
		tone(PIEZO,TONE_WRONG,TONE_WRONG_LEN);			//play the wrong tone sound
		PressCount = 0;									//and reset presscount to be ready to count again
	}
	

}  //end of main routine



bool DebounceSW(byte SWx)
{
	//Read the switch 5 times, 10mS apart.  Return 
	//the value of the majority of the reads.
	//this is to prevent false or erattic triggering
	//caused by the internal mechanical bounce of a switch

	byte HighCounts = 0;

	for (int x = 0; x <5; x++)
	{
		if (!digitalRead(SWx))    //invert the reading due to the use of pullups
		{
			HighCounts++;		//increment the high counter	
		}

		delay(10);				//wait 10 mS before looping again
	}
	return (HighCounts > 2);    //if there are more than 2 hights, return high
}




void RX_MQTT(char *topic, byte *payload, unsigned int length)
{

	char	Command;
	char	Relay;

	Serial.print("RXD Topic:   ");			//show the topic just for troubleshooting
	Serial.println(topic);
	Serial.print("Payload:  ");				//show the payload for troubleshooting

	for (int i = 0; i < length; i++) {
		Serial.print((char)payload[i]);		//loop through the payload, casting each byte to a char
	}										//to print it
	Serial.println("");

	Command = payload[0];

	switch (Command)
	{
	case COMMAND_ACTIVATE:

		//structure:  C,n,xxxxxxxx   C = command byte   n = relay number  xxxx = winning combo (up to 8 chars),
		
		Serial.println("Activate Command Received:");
		Serial.print("     Relay:    ");
		
		Relay = payload[2] - '0';			//get the relay nubmer, convert to actual value instead of a char
		Serial.println((byte)Relay);		//by subtracting ascii 0 from the value.  print using
											//byte to make sure the calc was correct
											//sending relay '1' will access relay [0] in code


		Serial.print("     To Win:    ");
		for (byte x = 4; x < length; x++)				//copy the winning sequence from payload
		{												//into winners array
			Winners[Relay][x-4] = payload[x];
			Serial.print((char)Winners[Relay][x-4]);	//this time, make sure the print out is using chars
			if (x == 11) break;							//as there can be letters and numbers
														//stop at 11th char in received string, which will be
														//the 8th character in the winning combo
		}
		Winners[Relay][length - 4] = 0;					//mark the end of the winnign combo with a 0	
		Serial.println(".");

		Active[Relay] = true;							//indicate that teh relay is now in play
		
		digitalWrite(INDICATOR, INDICATOR_ON);			//turn on the indicator (its dosn't matter if its already on)
		FindMax();										//find the longest active relay code

		break;

	case COMMAND_OPEN:

		//structure:  C,n		C = command byte   n = unit number to open

		Serial.println("Open Command Received:");
		Serial.print("     Relay:    ");


		Relay = payload[2] - '0';			//get the unit nubmer, convert to actual value instead of a char
		Serial.println((byte)Relay);		//by subtracting ascii 0 from the value.  print using
											//byte to make sure the calc was correct

		
		ReportWinner(Relay);
		
		break;

	case COMMAND_RESET:
		Serial.println("Reset Command Received");
		ResetGame();
		break;
	}
	
}

void ResetGame()
{	//reset the game to be ready to play.  Called at startup, and
	//in response to the reset command over MQTT

	for (byte x = 0; x < 4; x++)
	{
		digitalWrite(RELAYS[x], RELAY_OFF);
		Active[x] == false;
		RightCount[x] = 0;
	}

	digitalWrite(INDICATOR, INDICATOR_OFF);
	PressCount = 0;
	MaxLength = 0;
	
}

void FindMax()
{	//loop through the active relays and store the lenght 
	//of the longest winning sequence in the MaxLength variable
	
	MaxLength = 0;

	for (byte x = 0; x < 4; x++)
	{
		if (!Active[x]) continue;			//if the relay is not active, skip it - continue the FOR loop

		for (byte y=0; y<9; y++)				//now loop through the elements of the winners array and look for the 
		{									//null terminator (0)

			if(Winners[x][y] == 0)						//When the end (null) is found, 
			{											//see if the length is greather than the current maxlength
				if (y > MaxLength) MaxLength = y;	//if it is greater, store the new value in MaxLength
			}//end of if winners
		}//end of FOR y
	}//end of FOR x

	
}




void ReportWinner(byte Relay)
{	
	
	digitalWrite(RELAYS[Relay], RELAY_ON);					//turn on the appropriate relay
	Active[Relay] = false;									//remove the relay from the active game
	
	Serial.print("Won game for relay ");
	Serial.print(Relay);
	Serial.print("   (pin  ");
	Serial.print(RELAYS[Relay]);
	Serial.println(" )");

	PressCount = 0;
	FindMax();												//find the longest active relay

	tone(PIEZO,TONE_OPEN,TONE_OPEN_LEN);					//play the winning tone for the player
	
	//send in the winning MQTT report
	char Stat[] = "x,WON";									//create the structure for teh status message

	Stat[0] = Relay + '0';									//add 48 ('0') to get the ascii value of the relay number

	MQTTClient.publish(STATUS_TOPIC, Stat);					//send the message;

}

