#include <Tones.h>
#include <Ethernet2.h>
#include <EscapeClient.h>

/*

Time Selector

2017 Shawn Yates
shawn@cluecontrol.com

written and tested vMicro in Visual Studio

This is a prop controlled by MQTT messages.

The prop will have a 4 VFD displays, a relay output,
a piezo and two buttons.

*************************************************************************
* CAUTION * CAUTION * CAUTION * CAUTION * CAUTION * CAUTION * CAUTION   *
*************************************************************************
*   The VFD displpays draw 200mA each from the 5V line.  A typical		*
*   computer USB can only supply 500mA.  Do NOT connect the VFD			*
*   when programming the feather, it can damage your computers USB		*
*   port!!																*
*																		*
*   The power supplies used for RaspberryPI supply 2500mA to the USB	*
*   port and can be used to power the feather which in turn will power	*
*   the VFD.															*
**************************************************************************

One button advances which digit is being manipulated
the other button advancdes the digit through a
predefined set of values.

The piezo will chirp with each button press.

The VFD data is here:  
Web page with lots of pictures:  http://www.nixieclock.org/?p=411
Applicatoin Guide:	https://github.com/downloads/aguegu/nixie-tube/VFD_Tube_Module_IV-22_Application_Guide_v1.0.0_EN.pdf	

When the proper value is set into the 4 positions, the state
of the relay is changed.

The "proper value" is set by the incoming MQTT message that
activates the game for play.

At startup, the prop is dectivated, idle and listening
for commands from the MQTT server.

The prop will listen for commands on one topic (see code for topic name)
Expected commands are:

1,xxxx				Start the game
					xxxx is the winning sequence (digits 0 to 9)
					Currently must be 4 digits 

2					Show winning combo and win the game

3					Resets the game to be ready for next play

The prop will send updates on one topic (see code for topic name)

1,WON				Indicates the game has been won

the prop will send heartbeat messages on one topic (see code for topic name)

-------------------
*/

/*
// pin assignments
* Signal     Pin
* ------------------------
*	 0  Serial RX
*	 1  Serial TX
*	22	MISO - used to talk to ethernet board
*	23	MOSI - used to talk to ethernet board
*	24	SCK - used to talk to ethernet board
*	A5	Button A
*	A4	Button LEDs
*	A3	Button B
*	A2	
*	A1	Piezo
*	A0	


*	13	Relay (the onboard LED also on this pin)
*	12	VFD - Store Clock  
*	11	VFD - Shift Clock 
*	10	CS - used to talk to ethernet board
*	 9	not used *battery charger
*	 6	VFD - Output Enable   
*	 5	VFD - Data IN  
*	SCL	

*/
// Settings likely to change/be tweaked

char *COMMAND_TOPIC = "TimeSelect/Commands";		//topic to listen for commands
char *HB_TOPIC = "TimeSelect/HeartBeat";			//topic to send heartbeats
char *STATUS_TOPIC = "TimeSelect/Status";			//topic to send status
char *MY_MQTT_ID = "TimeSelect";					//id for this prop

byte MY_MAC[] = { 0xDE, 0x3D, 0xBA, 0xFE, 0xFF, 0xEF };		//MAC address for this prop.  MUST be unique on the network

															//note there is no '=' for creating IP addresses
IPAddress MY_IP(192, 168, 1, 6);							//IP address for this prop.  MUST be unique on the network
IPAddress BROKER_IP(192, 168, 1, 58);						//IP address of the MQTT broker


const long	COLORRATE = 1000;				//how many mS for the cycle when changing the backcolor of 
											//un-selected VFD tubes

const byte COLORTOL = 40;					//to produce random color change timing, the COLORRATE will be
											// +/- this value when calculating the next time to change each digits
											// color - lower number here means less randomness, higher means more 

const long BLINKRATE = 600;					//how fast to blink the currently selected digit

const long KEY_REPEAT_DELAY = 500;			//how many mS does the player hold the button down before it starts
											//scrolling faster

const long KEY_REPEAT_RATE = 10;			//how many mS between changes when scrolling in "fast scroll" mode

const bool	LED_ON = HIGH;					//for the LEDs in the buttons - are the active high or actived low?
const bool  RELAY_ON = HIGH;				//is the relay output active high or active low?

TONES Tones;								//enumarted tones for easy reading and changing


const unsigned int TONE_SELECT = Tones.NOTE_B6;
const unsigned int TONE_VALUE = Tones.NOTE_G4;


/****************************************************************************************
// Most things below this line shouldn't be changed unless you know what you are doing.	*
// but feel free to read, learn and experiment.											*
//***************************************************************************************/

// Pin Renames

const byte Relay = 13;					//used to control the relay
const byte SelectButton = A5;			//used to select which digit is active
const byte ChangeValueButton = A3;		//used to change the selected digit
const byte Piezo = A1;					//the piezo buzzer

const byte ButtonLEDs = A4;				//LEDs in the buttons

const byte	VFD_din = 5;				//pins to manipulate the VFD display
const byte	VFD_oen = 6;
const byte  VFD_sft = 11;
const byte  VFD_sto = 12;

// Other Constants

const byte COMMAND_ACTIVATE = '1';			//setup commands expected over MQTT
const byte COMMAND_OPEN = '2';				//note the single quotes store the ascii value of the char
const byte COMMAND_RESET = '3';

const bool	LED_OFF = !LED_ON;				//off is the opposite of ON.. who would have guessed!
const bool  RELAY_OFF = !RELAY_ON;

const byte NUMBERS[] = {// 0-9, off							//segment patterns for displaying numbers on the VFD
0xb7, 0x22, 0x9b, 0xab, 0x2e, 0xad, 0xbd, 0x23, 0xbf, 0xaf,0 };
//0    1     2     3     4     5     6     7     8     9   off(10);


//game states for each digit, for readability purposes
const byte LED_NOT_ACTIVE = 10;
const byte LED_BLINK_ON = 11;
const byte LED_BLINK_OFF = 12;
const byte LED_WON = 13;

// Function Prototypes

bool DebounceSW(byte SWx);											//Button debounce function
void RX_MQTT(char *topic, byte *payload, unsigned int length);		//Callback for MQQT receive
void ActivateDisplay();												//Start game play
void ResetGame();													//reset the game back to ready to play state
void CheckWinner();													//check to see if a game has been won
void ForceWinner();													//to force winning state based on MQTT command
void ReportWinner();												//report winner to MQTT server

void UpdateLED();													//Display current value on the LED
void ClearLED(bool ClearBG);										//clear (turn off) LED
void ClearDigit(byte Digit);										//clear (turn off) indicated digit
void UpdateBackcolor(byte Digit);									//set the backcolor of the digit to a random color


void CelebrateWin();												//display something cool when they win	


//Class Setup

EthernetClient ethClient;
PubSubClient pubSubClient(BROKER_IP, 1883, ethClient);				//create MQTT client with broker ip and default port of 1883
EscapeClient MQTTClient = EscapeClient();							//Create a client using the SteelOwl MQTT wrapper

//Variables

byte Winners[4] = { 1,2,3,4 };										//winning combos - will be changed by MQTT activation command

byte CurrentValue[4];												//hold the index of the currently diplayed
																	//character for each digit

byte SavedValue[4];													//to clear a digit, the currentvalue is changed
																	//store it here in SavedValue to turn the digit
																	//back on
byte CurrentColor[4];												//holds the background color for each VFD


byte CurrentDigit;													//used to store character/state of the display
																	//is currently selected.  Will also be used to 
																	//indicate various states by storing values
																	//0-3 = digit selection (blinking on)
																	//4-7 = digit selection (blinking off)
																	//10 = Display idle - not active yet
																	//11 = display just started, digits flashing ON
																	//12 = display just started, digits flashing OFF
																	//13 = game won, game inactive, maglock off

bool ButtonState[2];												//used to track the last known state of each button
																	//to prevent re-trigger of teh select button
																	//and to track hold time for value button to enable
																	//fast scrolling

unsigned long NextBGChange[4];										//used to hold the time to change the BG for each light
																	//slightly different on each light to give a random effect

unsigned long NextLEDEvent;											//used to store future millis value for the LED
																	//to take another action (blink on/off)

unsigned long NextButtonEvent;										//used to time the holding of the digit value button
bool FastScroll;													//indicate if the LED is in fast scroll mode

char MQTTStatus[10];												//used to send status message to MQTT

byte	WorkingValue;												//used for readability in code
byte	WorkingDigit;

void setup()
{

	// serial setup
	Serial.begin(9600);
	Serial.println("Serial interface started");

	MQTTClient.setup(pubSubClient, MY_MAC, MY_IP, MY_MQTT_ID, HB_TOPIC);	//setup the call back for
	Topic t = { COMMAND_TOPIC,RX_MQTT };										//create the topic subscription
	MQTTClient.registerChannel(t);											//and register to monitor it

	Serial.print("Expecting broker at ");
	Serial.println(BROKER_IP);

	pinMode(SelectButton, INPUT_PULLUP);
	pinMode(ChangeValueButton, INPUT_PULLUP);
	pinMode(Relay, OUTPUT);

	pinMode(VFD_din, OUTPUT);
	pinMode(VFD_oen, OUTPUT);
	pinMode(VFD_sft, OUTPUT);
	pinMode(VFD_sto, OUTPUT);


	digitalWrite(VFD_din, LOW);
	digitalWrite(VFD_sft, LOW);
	digitalWrite(VFD_sto, LOW);

	while (DebounceSW(SelectButton))		//Rund VFD test if teh select button is pressed at startup
	{										//keep running it till the button is let go
		for (byte y = 0; y < 11; y++)		//dislpay all 10 number values
		{
			for (byte x = 0; x < 4; x++)	// on all four lights
			{
				Send(y % 8);		//change the back color to the remainder of division by 8 (valid valeus are 0 to 7)
				Send(NUMBERS[y]);
			}

			digitalWrite(VFD_sto, HIGH);
			digitalWrite(VFD_sto, LOW);

			if (y == 0) 
			{
				digitalWrite(ButtonLEDs, LED_ON);
				digitalWrite(Relay, RELAY_ON);
			}

			if (y == 5) 
			{
				digitalWrite(ButtonLEDs, LED_OFF);
				digitalWrite(Relay, RELAY_OFF);
			}
			delay(300);

		}

	}

	
	ResetGame();
}

void loop()
{	

	MQTTClient.loop();					// service the MQTT client.  Keep in main loop all the time


		switch (CurrentDigit)
		{
			case LED_NOT_ACTIVE:
			case LED_WON:
				return;					

			case 0: case 1: case 2: case 3:					//if any digit is selected, turn LED off
				if (millis() > NextLEDEvent)				//when NextEvent happens - blinking the selected
				{											//digit
					ClearDigit(CurrentDigit);				//by turning it off here and back on below
					NextLEDEvent = millis() + BLINKRATE;
					CurrentDigit = CurrentDigit + 4;		//now it will be 4-7 and use the case below
				}
				break;


			case 4: case 5: case 6: case 7:
				if (millis() > NextLEDEvent)				//when NextEvent happens - blinking the selected
				{											//digit

					CurrentValue[CurrentDigit-4] = SavedValue[CurrentDigit-4]; //retrieve the saved value

					UpdateLED();							//turning it on here and off above
					NextLEDEvent = millis() + BLINKRATE;
					CurrentDigit = CurrentDigit - 4;		//now it will be 0-4 and use the case above
				}
				break;

			case LED_BLINK_ON:								//if the LED is on,
				if (millis() > NextLEDEvent)				//and NextEvent time has passed
				{

					ClearLED(false);						//turn off digits but not Background
					NextLEDEvent = millis() + BLINKRATE;	//set teh the NextEvent time
					CurrentDigit = LED_BLINK_OFF;			//and the state, so tehy will be turned
				}											//back on when time passes.
				break;

			case LED_BLINK_OFF:								//if LED is off
				if (millis() > NextLEDEvent)				//and NextEvent time has passed
				{
					for (byte x = 0;x<4;x++)
						CurrentValue[x] = 0;				//Assign 0 to each digit
					UpdateLED();							//turn the LED on
					NextLEDEvent = millis() + BLINKRATE;	//set teh the NextEvent time
					CurrentDigit = LED_BLINK_ON;			//and the state, so tehy will be turned
				}											//back off when time passes.
				break;



		}//end of swith based on state



		 /************************************************************/


		 //note that code below here 
		 //will not execute if the game is inacative or won
		 //because of the "RETURN" statement in the not active case above
		
		for (byte x = 0; x < 4; x++)				// check the background color timer for each digit, 
		{											// if any are past, update the backcolor

			if (CurrentDigit == x) continue;		//if x is the currently selected digit (blinking on or blinking off)
			if (CurrentDigit - 4 == x) continue;	//dont udpate the background color - leave it steady where it is

			if (millis() > NextBGChange[x]) UpdateBackcolor(x);		//if millis is over teh next BG time, update the BG color
																	//which also updates the NextBGChange for that digit
		}



		 //check the state of the buttons

		 //if button a is pressed, change to the next digit, don't do this again until
		 //the buton is first released

		if (DebounceSW(SelectButton) != ButtonState[0])				//if the button is other than the state
		{															//it used to be, process the state change

			ButtonState[0] = !ButtonState[0];						//the new state must be the opposite of the old

			if (ButtonState[0])										//if button is now pressed, increment digit
			{
				tone(Piezo, TONE_SELECT, 30);

				for (byte x = 0;x < 4;x++)
				{
					CurrentValue[x] = SavedValue[x];				//Make sure all lights are on
				}


				if (CurrentDigit > 9)								////if this is the first button push,
				{
					CurrentDigit = 255;								//make sure the ++ below starts at 0
				}

				CurrentDigit++;										//increment current digit
				if (CurrentDigit > 3) CurrentDigit -= 4;			//if its over 3, subtract 4
			}
		}

		//if button b is pressed, increment the digit value.  If the button is held,
		//increment the values faster until the button is released
		//when the button is released (afer a long or short press) send thew new current
		//value to the MQTT dispatcher  Also, check for a winning combination

		if (DebounceSW(ChangeValueButton) != ButtonState[1])		//if the button has changed state, process it
		{
			ButtonState[1] = !ButtonState[1];						//toggle the state indicator


			if (ButtonState[1])											//button is now pressed
			{
				if (FastScroll)											//Play a tone, make it shorter
				{														//in fast scroll mode
					tone(Piezo, TONE_VALUE, 5);
				}
				else
				{
					tone(Piezo, TONE_VALUE, 30);
				}
				WorkingDigit = CurrentDigit;							//get the current digit, 
				if (CurrentDigit > 3) WorkingDigit = CurrentDigit - 4;	//if its 4-7 (because of blinking)
																		//reduce it by 4 to get the real digit value	


				WorkingValue = SavedValue[WorkingDigit];			//store the current value in working value for easy reading
				WorkingValue++;										//increment the current value
		

				if (WorkingValue > 9)								//If the get to 10, start over at 0 
				{														
					WorkingValue = 0;		
				}													
				
				SavedValue[WorkingDigit] = WorkingValue;			//save a copy in savedvalue
				CurrentValue[WorkingDigit] = WorkingValue;

				NextButtonEvent = millis() + KEY_REPEAT_DELAY;			//assume the next repeat is "key_repeat_delay" away
				if (FastScroll)											//but if fast scroll is already enabled
				{
					NextButtonEvent = millis() + KEY_REPEAT_RATE;		//use the shorter delay instead
				}


			}//end of if button has become activated
			else
			{															//button is deactivated.  Send update to MQTT
				MQTTStatus[0] = '1';									
				MQTTStatus[1] = ',';
				for (byte i = 0; i < 4; i++)
				{
					MQTTStatus[i + 2] = char(SavedValue[i] + '0');				//build the status message with the current values
				}														//of the sequence
				MQTTStatus[7] = 0;										//null terminate the string

				MQTTClient.publish(STATUS_TOPIC, MQTTStatus);			//send the message;
				FastScroll = false;										//make sure fast scroll is turned off

				CheckWinner();
			}//end of if button has become deactivated

		}//end of if button state changed

		if (ButtonState[1])												//if the value button is active
		{
			UpdateLED();												//keep the LED on when changing  it

			if (millis() > NextButtonEvent)								//and the right amount of time has passed
			{
				ButtonState[1] = !ButtonState[1];						//toggle the state, which will force it to be re-detected
				FastScroll = true;										//as a button push next time around.  Set fast scroll
			}															//so a shorter time will be used to repeat it
		}
			

}  //end of main routine


void ActivateDisplay()
{
	//Start gameplay.
	//The sequence value have to have been set already

	for (byte x = 0; x < 4; x++)
	{
		CurrentValue[x] = 0;								//reset each digit value back to the first digit in the sequence
		SavedValue[x] = 0;
		CurrentColor[x] = random(0, 8);						//set background to a random color value 0 to 7 (upper bound is not included in results)
		UpdateBackcolor(x);									//call update backcolor to set timers
		
	}
	CurrentDigit = LED_BLINK_ON;					//set teh mode to be blinking
	UpdateLED();									//show the digits
	digitalWrite(VFD_oen, LOW);
	NextLEDEvent = millis() + BLINKRATE;			//set teh timer for the blinking

	digitalWrite(ButtonLEDs, LED_ON);				//turn off the button LEDs
}

bool DebounceSW(byte SWx)
{
	//Read the switch 5 times, 10mS apart.  Return 
	//the value of the majority of the reads.
	//this is to prevent false or erattic triggering
	//caused by the internal mechanical bounce of a switch

	byte HighCounts = 0;

	for (int x = 0; x <5; x++)
	{
		if (!digitalRead(SWx))    //invert the reading due to the use of pullups
		{
			HighCounts++;		//increment the high counter	
		}

		delay(10);				//wait 10 mS before looping again
	}
	return (HighCounts > 2);    //if there are more than 2 hights, return high
}

void RX_MQTT(char *topic, byte *payload, unsigned int length)
{

	char	Command;

	Serial.print("RXD Topic:   ");			//show the topic just for troubleshooting
	Serial.println(topic);
	Serial.print("Payload:  ");				//show the payload for troubleshooting

	for (int i = 0; i < length; i++) {
		Serial.print((char)payload[i]);		//loop through the payload, casting each byte to a char
	}										//to print it
	Serial.println("");

	Command = payload[0];

	switch (Command)
	{
	case COMMAND_ACTIVATE:

		//structure:  C,xxxx,aaaa  C = command byte   xxxx = winning combo,
		//aaa = sequence to rotate (up to 20 chars)

		Serial.println("Activate Command Received:");

		Serial.print("     To Win:    ");
		for (byte x = 0; x < 4; x++)				//copy the winning sequence from payload
		{											//into winners array
			Winners[x] = payload[x + 2] - 48;
			Serial.print(Winners[x]);			//this time, make sure the print out is using chars
		}											//as there can be letters and numbers
		Serial.println();
		
		randomSeed(millis());

		ActivateDisplay();
		
		
		break;

	case COMMAND_OPEN:

		//structure:  C,n		C = command byte   n = unit number to open

		Serial.println("Open Command Received:");
		Serial.print("     Drawer:    ");
		
		ForceWinner();
		break;

	case COMMAND_RESET:
		Serial.println("Reset Command Received");
		ResetGame();
		break;
	}
	
}

void ResetGame()
{	//reset the game to be ready to play.  Called at startup, and
	//in response to the reset command over MQTT

	//make sure the displays are blank/off
	
	for (byte x = 0; x < 3; x++)				//set each LED set to inactive state
	{
		CurrentDigit = LED_NOT_ACTIVE;
		digitalWrite(Relay, RELAY_OFF);
	}
	digitalWrite(ButtonLEDs, LED_OFF);		//turn off the button LEDs
	ClearLED(true);
	digitalWrite(VFD_oen, HIGH);
	
}

void ClearLED(bool ClearBG)								//turn off all digits, if NoBG is true, turn off backgrounds
{
	for (byte x = 0; x < 4; x++)
	{
		if (ClearBG) CurrentColor[x] = 7;				//set BG color to 0 and display segments to 0 for all 4 LED

		CurrentValue[x] = 10;							//index 10 has no segments turned on
	}
	UpdateLED();
}


void ClearDigit(byte Digit)								//clear (turn off) indicated digit on indicated LED
{
	CurrentValue[Digit] = 10;								//index 10 has no segments turned on
	UpdateLED();
}


void UpdateLED()												//display all 4 digits
{	
	for (byte x = 0; x < 4; x++)	
	{	
		//Serial.print(x);
		//Serial.print(" - ");
		//Serial.print(CurrentColor[x]);
		//Serial.print(" : ");
		//Serial.print(CurrentValue[x]);
		//Serial.print(" :: ");
		//Serial.println(NUMBERS[CurrentValue[x]],BIN);

		
		Send(CurrentColor[x]);
		Send(NUMBERS[CurrentValue[x]]);							//get the value for each digit
																//CurrentValue[x] holds the index of
																//the digit to be displayed
	}
	Serial.println("");

	digitalWrite(VFD_sto, HIGH);
	digitalWrite(VFD_sto, LOW);

}

void Send(byte dat)
{
	for (byte i = 8; i > 0; i--)
	{
		digitalWrite(VFD_din, bitRead(dat, i - 1));
		
		digitalWrite(VFD_sft, HIGH);
		digitalWrite(VFD_sft, LOW);

	}

}

void UpdateBackcolor(byte Digit)
{
	//set the background to a new color (0 to 7)
	//Set the next change to a slightly random value based on parameters
	
	CurrentColor[Digit]++;
	if (CurrentColor[Digit] > 7) CurrentColor[Digit] = 0;
	NextBGChange[Digit] = millis() + random((COLORRATE - COLORTOL), (COLORRATE+COLORTOL));

	UpdateLED();		//update the LED to show the new background color
}

void ForceWinner()
{		//force the game to a winning state by setting each
		//current value to the proper winning value

	for (byte i = 0; i < 4; i++)						//loop through each winning char and find it in sequence
	{
		CurrentValue[i] = Winners[i];
		SavedValue[i] = Winners[i];
	}

	CheckWinner();
}

void CheckWinner()
{		//check each char of the indicated unit to see if teh player won

		//note that the winners array is holding chars, so these must be 
		//matched up to the chars in the sequence array

	for (byte i = 0; i < 4; i++)			//go through the 4 digits.  
	{

		//CurrentValue[Unit][i] returns the index of teh currently displayed value
		//in the sequence.  If that does not match the same value in winners, exit
		if (CurrentValue[i] != Winners[i]) return;
	}

	//if the code above does not execute the return. all digits match, we have a winner!


	CurrentDigit = LED_WON;						//and set the state to won

	digitalWrite(Relay, RELAY_ON);
	CelebrateWin();
	ReportWinner();
	UpdateLED();									//update the display

	digitalWrite(ButtonLEDs, LED_OFF);		//turn off the button LEDs

}

void ReportWinner()
{	//send in the winning MQTT report
	char Stat[] = "1,WON";

	MQTTClient.publish(STATUS_TOPIC, Stat);			//send the message;

}

void CelebrateWin()

{	

	CurrentColor[1] = CurrentColor[0];
	UpdateLED();
	delay(500);

	CurrentColor[2] = CurrentColor[0];
	UpdateLED();
	delay(500);

	CurrentColor[3] = CurrentColor[0];
	UpdateLED();
	delay(500);

	for (byte x = 0; x < 7; x++)
	{
		for (byte y = 0; y < 4; y++)
		{
			CurrentColor[y] = x;
			CurrentValue[y] = SavedValue[y];
		}
		UpdateLED();
		delay(500);
	}



	ClearLED(false);
}
