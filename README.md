# README #

This repo has projects that were worked on or created by Shawn
shawn@cluecontrol.com

Most Feather or Arduino projects are created using VisualStudio.

As with traditional Arduino projects, the main files are the
.INO, .H and .CPP.  Other files are most likely VisualStudio
support files.

Each project that has a NodeRed setup will also contain a folder
called NodeRed where an export of the NodeRed setup for that prop
will be stored.

I am very open to feedback and improvement so feel free to look
at the code and make suggestions or observations.  And if there is
anything you see that you want to learn about, please ask, I really
like helping others.

Please feel free to email shawn@cluecontrol.com.