
#include <Adafruit_NeoPixel.h>
#include <Ethernet2.h>
#include <EscapeClient.h>
#include "wiring_private.h" // pinPeripheral() function



/*

Simon Bells Game

2017 Shawn Yates
shawn@cluecontrol.com

written and tested vMicro in Visual Studio

This is a prop controlled by MQTT messages.

The prop will have a main relay output, a pieze and 
4 buttons, each with an LED

In idle mode, the buttons and lights are inactive 
and the main output is inactive.

When an MQTT command is received to activate the game,
the prop will create a random sequence of the four buttons.
It will then play that sequence, lighting the light and playing 
a tone for each button in the sequence.

To win, players must repeat the pattern.
If players press a wrong button in the pattern, 
the pattern is replayed and the game starts over (not changing the patter)
Also an MQTT status message is sent.

When player win, an MQTT status is sent and the main output
is activatedd.  The buttons will no longer be monitored.

A reset MQTT message will reset the game back to Idle.  A new
start game message will start a new game.

At startup, the prop is dectivated, idle and listening
for commands from the MQTT server.

The prop will listen for commands on one topic (see code for topic name)
Expected commands are:

1,n			Start the game with n steps in the winning pattern
			n can be 1 to 8, inclusive

2			Force the game to the "won" state

3			Resets the game to idle

The prop will send updates on one topic (see code for topic name)

1,xxxx			where xxxx is winning pattern (up to 8 digits)
2				Indicates the game has been won
3				Indicates a wrong pattern was entered and players are restarting

the prop will send heartbeat messages on one topic (see code for topic name)

-------------------
*/

/*
// pin assignments
* Signal     Pin
* ------------------------
*	 0				Serial RX
*	 1				Serial TX
*	22				MISO - used to talk to ethernet board
*	23				MOSI - used to talk to ethernet board
*	24				SCK - used to talk to ethernet board
*	A5	Serial to MP3 - must be this pin because it must be a different port from the LEDs
*	A4  Button on Bell 4
*	A3	LED on bell 4
*	-A2				(can NOT be LED strip)
*	-A1				(can NOT be LED strip)
*	A0	


*	13				(the onboard LED also on this pin)
*	12		LED on Bell 3
*	11		Button on Bell 3
*	10				CS - used to talk to ethernet board
*	 9				not used *battery charger
*	 6		LED on Bell 2
*	 5		Button on Bell 1
*	SCL(21)	LED on Bell 1
*   SDA(20) Button on Bell 1

*/
// Settings likely to change/be tweaked

//MQTT Settings
	char *COMMAND_TOPIC =	"SimonBells/Commands";				//topic to listen for commands
	char *HB_TOPIC =		"SimonBells/HeartBeat";			//topic to send heartbeats
	char *STATUS_TOPIC =	"SimonBells/Status";				//topic to send status
	char *MY_MQTT_ID =		"SimonBells";						//id for this prop

	byte MY_MAC[] = { 0xDE, 0x47, 0xB6, 0xFB, 0xAE, 0x51 };		//MAC address for this prop.  MUST be unique on the network

															//note there is no '=' for creating IP addresses
	IPAddress MY_IP(192, 168, 1, 218);							//IP address for this prop.  MUST be unique on the network
	IPAddress BROKER_IP(192, 168, 1, 58);						//IP address of the MQTT broker


//LED Color Values
	//colors can be any value from 0x000000 to 0xFFFFFF  The values and names used here
	//are for easy reference.  They can be adjusted as needed.

	const uint32_t COLOR_RED			= 0xFF0000;		// Red
	const uint32_t COLOR_ORANGE			= 0xD52A00;	// Orange
	const uint32_t COLOR_OR_YELLOW		= 0xAB5500;	// Orange/Yellow
	const uint32_t COLOR_YELLOW			= 0xAB7F00;	// Yellow
	const uint32_t COLOR_YEL_GREEN		= 0xABAB00;	// Yellow/Green
	const uint32_t COLOR_GREEN			= 0x56D500;	// Green
	const uint32_t COLOR_GREEN_BLUE		= 0x00FF00;	// Green/BLue
	const uint32_t COLOR_LT_BLUE		= 0x00D52A;	// Light Blue
	const uint32_t COLOR_MED_BLUE		= 0x00AB55;	// Medium Blue
	const uint32_t COLOR_BLUE			= 0x0056AA;	// Blue
	const uint32_t COLOR_DARK_BLUE		= 0x0000FF;	// Blue
	const uint32_t COLOR_LT_PURPLE		= 0x5500AB;	// Light Purple
	const uint32_t COLOR_PURPLE			= 0x7F0081;	// Purple
	const uint32_t COLOR_PURPLE_PINK	= 0xAB0055;	// Purple Pink
	const uint32_t COLOR_PINK			= 0xD5002B;	// Pink

//Simon Settings
	const byte SIMON_BUTTON_COUNT = 4;									//how many physical buttons/bells are there
	const byte MAX_SIMON_STEPS = 10;									//What is max # of steps to have in simon pattern

	//It works best to name the files on the MP3 card with two digits as the first part of their name
	//the rest of the name can be anything.  Those first two digits should corospond to the files index
	//so the first file is 01xxxx.mp3, the second file is 02xxxx.mp3.  
	//Play files by passing the index number, that is if you count the files, which file number are you
	//trying to play.

	const byte SIMONSONGS[SIMON_BUTTON_COUNT] = {1 , 2, 3, 4 };	 
	const byte LOSER_SONG = 10;
	const byte WINNER_SONG = 11;
	
	//setup colors for the buttons and for the winning and losing sequences
	const uint32_t	SIMONCOLORS[SIMON_BUTTON_COUNT] = { COLOR_PURPLE, COLOR_DARK_BLUE, COLOR_ORANGE, COLOR_LT_BLUE };
	const uint32_t LOSER_COLOR = COLOR_RED;
	const uint32_t WINNER_COLOR = COLOR_GREEN;

// Most things below this line shouldn't be changed unless you know what you are doing.
// but feel free to read, learn and experiment.

//***************************************************************************

//Pin Definitions

	const byte SIMON_BUTTONS[SIMON_BUTTON_COUNT] = { 20,5,11,A4 };		//pins for buttons - must line up with pins for lights
	const byte SIMON_LEDS[SIMON_BUTTON_COUNT] = { 21,6,12,A3 };

	const byte LED_COUNT = 6;											//how many LEDs are on each strip in each bell
		
//Variables

	byte CurState = 0;							//track the current state of the game

	byte SimonSize;								//stores how many steps are in the current game
	
	byte SimonSteps[MAX_SIMON_STEPS] = { 0 };	//used to store the correct steps of the simon game
	byte CurSimonStep = 0;						//which step of simon is the player on?
	byte CurSimonRound;
	
	char MQTTStatus[MAX_SIMON_STEPS+3];			//used to send status message to MQTT


// MQTT Commands and Status Updates

	const byte COMMAND_ACTIVATE = '1';				//setup commands expected over MQTT
	const byte COMMAND_FORCEWIN = '2';				//note the single quotes store the ascii value of the char
	const byte COMMAND_RESET = '3';

	const byte REPORT_PATTERN = '1';
	const byte REPORT_WON = '2';
	const byte REPORT_LOST = '3';
	

// Function Prototypes

	bool DebounceSW(byte SWx);											//Button debounce function
	void RX_MQTT(char *topic, byte *payload, unsigned int length);		//Callback for MQQT receive
	void GeneratePattern(byte PatternLen);								//generate a new simon pattern
	void ResetGame();													//reset the game back to ready to play state
	void ShowSimon();													//used to show the players the simon sequence they need to repeat
	void GameWon();														//executed when players win
	void StripOn(byte StripID, uint32_t Color);							//turn on the indicated strip to the indicated color
	void PlaySound(byte SongIndex);										//play a sound on the MP3 board
	void sendCommand(int8_t command, int16_t dat);						//utility function for playing sounds on MP3 board

//Class Setup
	EthernetClient ethClient;
	PubSubClient pubSubClient(BROKER_IP, 1883, ethClient);				//create MQTT client with broker ip and default port of 1883
	EscapeClient MQTTClient = EscapeClient();							//Create a client using the SteelOwl MQTT wrapper
	Adafruit_NeoPixel BELL_LEDS[SIMON_BUTTON_COUNT];					//make an array of the pixels

	//create UART ports for talking to the MP3 player
	//because it must be on a different port from the LED drivers

	// (Sercom funcions, RX_PIN, TX_PIN, RX_PAD, TX_PAD)
	// see https://learn.adafruit.com/using-atsamd21-sercom-to-add-more-spi-i2c-serial-ports/creating-a-new-serial
	// for details

	// note that only the TX pin and pad will be used, the RX
	// will not be used
	Uart MP3(&sercom5, 3, A5, SERCOM_RX_PAD_1, UART_TX_PAD_0);

void setup()
{
		
	// serial setup
	MP3.begin(9600);
	pinPeripheral(A5, PIO_SERCOM_ALT);
		
	Serial.begin(9600);			//Serial to computer over USB

	delay(1000);
	Serial.println("Serial interface started");

	MQTTClient.setup(pubSubClient, MY_MAC, MY_IP, MY_MQTT_ID, HB_TOPIC);	//setup the call back for
	Topic t = { COMMAND_TOPIC,RX_MQTT };										//create the topic subscription
	MQTTClient.registerChannel(t);											//and register to monitor it

	Serial.println();
	Serial.print("My IP: ");
	Serial.println(MY_IP);

	Serial.print("Expecting dispatcher on ");
	Serial.println(BROKER_IP);

	Serial.print("Connected? = ");
	Serial.println(pubSubClient.connected());
	Serial.println();


	for (byte x = 0; x < SIMON_BUTTON_COUNT; x++)			//set the pin states
	{
		//this line setting up the NeoPixel must come before the pinMode for the pin used
		//by the NeoPixel.

		// Parameter 1 = number of pixels in strip
		// Parameter 2 = pin number (most are valid)
		// Parameter 3 = pixel type flags, add together as needed:
		//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
		//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
		//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
		//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
		BELL_LEDS[x]= Adafruit_NeoPixel(LED_COUNT, SIMON_LEDS[x], NEO_GRB + NEO_KHZ800);
		
		
		pinMode(SIMON_BUTTONS[x], INPUT_PULLUP);			//buttons are inputs, with pullup
		pinMode(SIMON_LEDS[x], OUTPUT);						//lights are outputs
		
	}


	ResetGame();

	Serial.println("Starting Main Loop");
	
}


void loop()
{

	
	MQTTClient.loop();					//service the MQTT client.  Keep in main loop all the time
	
	switch (CurState)
	{
	case 0:	// Do nothign here, this is idle state

		break;

	case 1:	//game in progress, waiting for player to press a button

		for (byte x = 0; x < SIMON_BUTTON_COUNT; x++)     //scan the Simon buttons for a press
		{
			if (!DebounceSW(SIMON_BUTTONS[x])) continue;	//if button is not pressed, go to next button
			
			//only executes from here down if the current button is pressed

			if (x != SimonSteps[CurSimonStep])			//if X != the next step then the player pushed
			{											//the wrong button
				
				
				MQTTStatus[0] = REPORT_LOST;		//Send a lost status update
				MQTTStatus[1] = 0;					//null terminate the string

				MQTTClient.publish(STATUS_TOPIC, MQTTStatus);			//send the message;

			

				for (byte Counter = 0; Counter < 3; Counter++)				//flash the lights and play the noise this many times
				{
					PlaySound(LOSER_SONG);									//play the losing song
					for (byte y = 0; y < SIMON_BUTTON_COUNT; y++)			//turn all the bells on with the "loser color"
						StripOn(y, LOSER_COLOR);

					delay(500);						

					for (byte y = 0; y < SIMON_BUTTON_COUNT; y++)			//turn off all the bell lights
						StripOn(y, 0);

					delay(200);
				}
				
				delay(2000);						//pause before re-showing them the right pattern

				CurSimonRound = 1;					//start over with just one light
				GeneratePattern(SimonSize);			//and make a new pattern

				ShowSimon();						//remind them of the sequence
				return;								//if they pushed the wrongbutton, return to main loop
			}
				
			//the pushed the right button - if they didn't, the return above will have been executed

			PlaySound(SIMONSONGS[x]);				//play the sound and 
			StripOn(x, SIMONCOLORS[x]);				//blink the light they pushed
			delay(300);									
			
			StripOn(x, 0);							//turn off the LED strip

			CurSimonStep++;								// Move to the next step

			if (CurSimonStep < CurSimonRound)				//if they have not yet hit the end of the sequence, keep playing
			{
				Serial.print("Next input pin: ");
				Serial.println(SimonSteps[CurSimonStep]);
				return;								//note that if they have not hit the end, this line returns
			}

			//if all steps are done, execute code below

			CurSimonRound++;					//increment the step counter

			if (CurSimonRound > SimonSize)		//if the current round number is now > the size, they won the game
			{
				GameWon();						
				return;
			}

			delay(1000);						//amll pause

			ShowSimon();						

		}//end of for loop which is scanning buttons;
		break;
		
	}// end of main switch statement

}

bool DebounceSW(byte SWx)
{
	//Read the switch 5 times, 10mS apart.  Return 
	//the value of the majority of the reads.
	//this is to prevent false or erattic triggering
	//caused by the internal mechanical bounce of a switch

	byte HighCounts = 0;

	for (int x = 0; x <5; x++)
	{
		if (!digitalRead(SWx))    //invert the reading due to the use of pullups
		{
			HighCounts++;		//increment the high counter	
		}
		delay(10);				//wait 10 mS before looping again
	}
	return (HighCounts > 2);    //if there are more than 2 hights, return high
}

void ShowSimon()
{

	MQTTStatus[0] = REPORT_PATTERN;								//setup the MQTT message
	MQTTStatus[1] = ',';

	for (byte x = 0; x < CurSimonRound; x++)
	{
		CurSimonStep = SimonSteps[x];							//get the value of the next right step

		MQTTStatus[x + 2] = SimonSteps[x] + 48;					//send the ascii value of the character

		PlaySound(SIMONSONGS[CurSimonStep]);					//play the sound and 
		StripOn(CurSimonStep, SIMONCOLORS[CurSimonStep]);		//turn on the light
		
		delay(400);												//delay for the light and sound
		
		StripOn(CurSimonStep, 0);								//turn the light back off
		
		delay(150);												//leave a little delay between steps
		
	}
	
	MQTTStatus[SimonSize +2] = 0;							//null terminate the string
	MQTTClient.publish(STATUS_TOPIC, MQTTStatus);			//send the message;

	Serial.print("Next input pin: ");
	Serial.println(SimonSteps[0]);

	CurSimonStep = 0;										//and reset the right step back to 0


}

void RX_MQTT(char *topic, byte *payload, unsigned int length)
{

	char	Command;
	char	Unit;

	Serial.print("RXD Topic:   ");			//show the topic just for troubleshooting
	Serial.println(topic);
	Serial.print("Payload:  ");				//show the payload for troubleshooting

	for (int i = 0; i < length; i++) {
		Serial.print((char)payload[i]);		//loop through the payload, casting each byte to a char
	}										//to print it
	Serial.println("");

	Command = payload[0];

	switch (Command)
	{
	case COMMAND_ACTIVATE:

		//structure:  C,n	C = command byte   n = number of digits in pattern
		

		Serial.println("Activate Command Received");

		SimonSize = payload[2] - 48;			//get the unit nubmer, convert to actual value instead of a char
		
		if (SimonSize > MAX_SIMON_STEPS) SimonSize = MAX_SIMON_STEPS;

		Serial.print((byte)SimonSize);		//by subtracting ascii 0 from the value.  print using
											//byte to make sure the calc was correct

		Serial.println(" steps requested.");

		ResetGame();

		CurState = 1;
		CurSimonRound = 1;

		GeneratePattern(SimonSize);
		ShowSimon();

		
		break;

	case COMMAND_FORCEWIN:

		//structure:  C				C = command byte

		Serial.println("Force Win Command Received");
		GameWon();
		break;

	case COMMAND_RESET:
		Serial.println("Reset Command Received");
		ResetGame();
		break;
	}




}

void GeneratePattern(byte PatternLen)			//generate a new simon pattern of PatternLen steps
{
	
	randomSeed(millis());						//make sure we are randomized

	Serial.print("New winning pattern: ");


	for (byte x = 0; x < PatternLen; x++)
	{
		SimonSteps[x] = random(0, SIMON_BUTTON_COUNT);	//generate the simon steps.  Note that the upper bound of the random
														//is exclusive, so if simon count = 5, it will return 0 to 4

		//dont let the same button be used more than twice in a row

		if (x > 1)		//start checking when on x=2 and greater (0 and 1 are filled in)
		{
			if (SimonSteps[x - 1] == SimonSteps[x - 2])			//if the last two steps are the same
			{
				while (SimonSteps[x] == SimonSteps[x - 1])		//if the new step is the same as the last step
				{
					SimonSteps[x] = random(0, SIMON_BUTTON_COUNT);	//generate a new value for the current step
				}
			}

		}

		Serial.print(SimonSteps[x]);
		Serial.print(" ");
	}

	Serial.println("");

	CurSimonStep = 0;

}

void GameWon()
{	//do something fun when the players win the game

	Serial.println("Game Won!");
	PlaySound(WINNER_SONG);

	for (byte x = 0; x < SIMON_BUTTON_COUNT; x++)
	{
		StripOn(x, SIMONCOLORS[x]);
		delay(300);
	}

	delay(200);

	for (byte x = 0; x < SIMON_BUTTON_COUNT; x++)
	{
		StripOn(x, WINNER_COLOR);
		delay(300);
	}

	
	MQTTStatus[0] = REPORT_WON;			//Send a won status update
	MQTTStatus[1] = 0;					//null terminate the string

	MQTTClient.publish(STATUS_TOPIC, MQTTStatus);			//send the message;



	CurState = 0;						//and return to idle mode

}

void StripOn(byte StripID, uint32_t Color) 
{

	//turn on all the lights on the strip to the indicated color
	for (byte x = 0; x < LED_COUNT; x++)
	{
		BELL_LEDS[StripID].setPixelColor(x, Color);
	}
	BELL_LEDS[StripID].show();
	
}

void ResetGame()
{	//reset the game to be ready to play.  Called at startup, and
	//in response to the reset command over MQTT

	for (byte x = 0; x < SIMON_BUTTON_COUNT; x++)
	{
		StripOn(x, 0);									//make sure all lights are off

	}


	CurSimonStep = 0;									//clear current simon step and current state
	CurState = 0;
	CurSimonRound = 0;

}

void PlaySound(byte SongIndex)
{

	//command structure
	/*
	* Mark     Byte          Byte description
	$S       0x7E          Every command should start with $(0x7E)
	VER       0xFF          Version information
	Len       0xxx          The number of bytes of the command without starting byte and ending byte
	CMD       0xxx          Such as PLAY and PAUSE and so on
	Feedback  0xxx          0x00 = not feedback, 0x01 = feedback
	data                    The length of the data is not limit and usually it has two bytes
	$O        0xEF          Ending byte of the command
	*
	*/
	/************Command byte**************************/
#define CMD_NEXT_SONG         0X01
#define CMD_PREV_SONG         0X02
#define CMD_PLAY_W_INDEX      0X03
#define CMD_VOLUME_UP         0X04
#define CMD_VOLUME_DOWN       0X05
#define CMD_SET_VOLUME        0X06
#define CMD_SINGLE_CYCLE_PLAY 0X08
#define CMD_SEL_DEV           0X09
#define DEV_TF                0X02
#define CMD_SLEEP_MODE        0X0A
#define CMD_WAKE_UP           0X0B
#define CMD_RESET             0X0C
#define CMD_PLAY              0X0D
#define CMD_PAUSE             0X0E
#define CMD_PLAY_FOLDER_FILE  0X0F
#define CMD_STOP_PLAY         0X16
#define CMD_FOLDER_CYCLE      0X17
#define CMD_SET_SINGLE_CYCLE  0X19
#define SINGLE_CYCLE_ON       0X00
#define SINGLE_CYCLE_OFF      0X01
#define CMD_SET_DAC           0X1A
#define DAC_ON                0X00
#define DAC_OFF               0X01
#define CMD_PLAY_W_VOL        0X22 

	/*
	*
	*[Next Song]        7E FF 06 01 00 00 00 EF
	[Previous Song]     7E FF 06 02 00 00 00 EF
	[Play with index]   7E FF 06 03 00 00 01 EF   Play the first song (01)
						7E FF 06 03 00 00 02 EF   Play the second song (02)
	[Volume up]         7E FF 06 04 00 00 00 EF   Volume increased one
	[Volume down]       7E FF 06 05 00 00 00 EF   Volume decrease one
	[Set volume]        7E FF 06 06 00 00 1E EF   Set the volume to 30 (0x1E is 30)
	[Single cycle play] 7E FF 06 08 00 00 01 EF   Single cycle play the first song
	[Select device]     7E FF 06 09 00 00 02 EF   Select storage device to TF card
	[Sleep mode]        7E FF 06 0A 00 00 00 EF   Chip enters sleep mode
	[Wake up]           7E FF 06 0B 00 00 00 EF   Chip wakes up
	[Reset]             7E FF 06 0C 00 00 00 EF   Chip reset
	[Play]              7E FF 06 0D 00 00 00 EF   Resume playback
	[Pause]             7E FF 06 0E 00 00 00 EF   Playback is paused
	[Play file name]    7E FF 06 0F 00 01 01 EF   Play the song with the directory: /01/001xxx.mp3
						7E FF 06 0F 00 01 02 EF   Play the song with the directory: /01/002xxx.mp3
	[Stop play]         7E FF 06 16 00 00 00 EF
	[Cycle folder name] 7E FF 06 17 00 01 02 EF   01 folder cycle play
	[Set 1 cycle play]  7E FF 06 19 00 00 00 EF   Start up single cycle play
						7E FF 06 19 00 00 01 EF   Close single cycle play
	[Set DAC]           7E FF 06 1A 00 00 00 EF   Start up DAC output
	7E FF 06 1A 00 00 01 EF   DAC no output
	[Play with volume]  7E FF 06 22 00 1E 01 EF   Set the volume to 30 (0x1E is 30) and play the first song
						7E FF 06 22 00 0F 02 EF   Set the volume to 15(0x0f is 15) and play the second song
	*/

	sendCommand(CMD_SEL_DEV, DEV_TF);

	delay(200);

	sendCommand(CMD_PLAY_W_VOL, 0X1E00 + SongIndex);    //volume 1E = 30(max) 00 is where song # goes
														//adding the value of SongIndex will fill it in 
}


void sendCommand(int8_t command, int16_t dat)
{
	static int8_t Send_buf[8] = { 0 };
	Send_buf[0] = 0x7e; //
	Send_buf[1] = 0xff; //
	Send_buf[2] = 0x06; //
	Send_buf[3] = command; //
	Send_buf[4] = 0x00;//
	Send_buf[5] = (int8_t)(dat >> 8);//datah
	Send_buf[6] = (int8_t)(dat); //datal
	Send_buf[7] = 0xef; //

	for (uint8_t i = 0; i<8; i++)//
	{
		MP3.write(Send_buf[i]);
	}
}

